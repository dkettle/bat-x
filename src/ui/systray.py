import sys
import os
try:
    from PyQt5.QtGui import QIcon
    from PyQt5.QtWidgets import (QAction, QApplication, QCheckBox, QComboBox,
        QDialog, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit,
        QMessageBox, QMenu, QPushButton, QSpinBox, QStyle, QSystemTrayIcon,
        QTextEdit, QVBoxLayout)
    from . import imgs

except ImportError as iE:
    print("Failed to launch battery system tray, PyQt5 library not found")


app = QApplication([])
app.setQuitOnLastWindowClosed(False)

if not QSystemTrayIcon.isSystemTrayAvailable():
    QMessageBox.critical(None, "Systray",
            "It appears the systemtray feature is not available on this system")
    sys.exit(1)

class SysTray(QSystemTrayIcon):

    def __init__(self):
        super().__init__()
        self.setIcon(QIcon(
            os.path.join(os.path.dirname(imgs.__file__), "battery.png")))
        self.setVisible(True)

        menu = QMenu()
        menu.addAction(QIcon(
            os.path.join(os.path.dirname(imgs.__file__), "quit.png")),
            "Quit", app.exit)
        self.setContextMenu(menu)

tray = SysTray()
app.exec_()
