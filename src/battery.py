import os

SYS_POWER_SUPPLY = os.path.join("sys", "class", "power_supply")

class PollingBattery():
    pass

class InotifyBattery():
    pass

class StaticBattery():
    """
    :Description:

        Battery class with properties that access attributes, no monitoring

    """
    def __init__(self, name):
        self.name = name
        self.path = os.path.join(SYS_POWER_SUPPLY, self.name)
        if not os.path.exists(self.path):
            raise BatteryNotFoundException("{} not found".format(self.name))

    @property
    def capacity(self):
        path = os.path.join(self.path, "capacity")
        if os.path.exists(path):
            with open(self.path) as f:
                capacity = f.read().strip()
                return int(capacity)

    @property
    def manufacturer(self):
        path = os.path.join(self.path, "manufacturer")
        if os.path.exists(path):
            with open(self.path) as f:
                return f.read().strip()

    @property
    def model_name(self):
        path = os.path.join(self.path, "model_name")
        if os.path.exists(path):
            with open(self.path) as f:
                return f.read().strip()

    @property
    def energy_full(self):
        path = os.path.join(self.path, "energy_full")
        if os.path.exists(path):
            with open(self.path) as f:
                return f.read().strip()

    @property
    def energy_now(self):
        path = os.path.join(self.path, "energy_now")
        if os.path.exists(path):
            with open(self.path) as f:
                return f.read().strip()

    @property
    def status(self):
        path = os.path.join(self.path, "status")
        if os.path.exists(path):
            with open(self.path) as f:
                return f.read().strip()


    @property
    def voltage_now(self):
        path = os.path.join(self.path, "voltage_now")
        if os.path.exists(path):
            with open(self.path) as f:
                return f.read().strip()


class ThinkpadBattery():
    pass

class DellBattery():
    pass
