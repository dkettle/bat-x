#!/usr/bin/env python3
"""
Runs the main service and lists useful arguments for additional information
"""
import sys
import argparse
import json
from .lib import devices

def main(args):
    if args.list_power_devices:
        d = devices.list_power_devices()
        print("\n".join(d.keys()))

    elif args.list_batteries:
        b = devices.list_batteries()
        print("\n".join(b.keys()))

    elif args.battery_details:
        b = devices.battery_details()
        for key, value in b.items():
            print(key)
            print(json.dumps(value, indent=4, sort_keys=True))

    if not args.battery is None:
        b = devices.battery_detail(args.battery)
        print(args.battery)
        print(json.dumps(b, indent=4, sort_keys=True))

    if args.gui:
        from .ui import systray


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Bat-X Battery System Daemon')
    parser.add_argument('--list-power-devices', action="store_true",
                        help='List all power devices from sysfs')
    parser.add_argument('-l', '--list-batteries', action="store_true",
                        help='List all battery devices from sysfs')
    parser.add_argument('-d', '--battery-details', action="store_true",
                        help="Display details for all batteries detected")
    parser.add_argument('-b', '--battery',
                        help='Display battery details. '
                            'Ex: "BAT0", as it appears in /sys')
    parser.add_argument('-g', '--gui', help='Launch battery monitor interface',
                        action='store_true', default=False)
    parser.add_argument('--no-gui', help='Do not launch battery monitor interface'
                        ' (default behaviour)', dest='gui', action='store_false')

    main(parser.parse_args())
    sys.exit(0)
