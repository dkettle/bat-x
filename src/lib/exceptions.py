class BatteryException(Exception):
    pass

class BatteryParameterUnreadableException(BatteryException):
    pass

class SysFsNotFound(Exception):
    pass

class BatteryNotFound(BatteryException):
    pass
