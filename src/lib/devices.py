from .exceptions import SysFsNotFound, BatteryNotFound
import os

SYS_FS_DEVICES_TREE = os.path.join(os.sep, "sys", "class", "power_supply")
if not os.path.exists(SYS_FS_DEVICES_TREE):
    raise SysFsNotFound("Could not find directory containing power devices")

FILES_TO_READ       = [
    "capacity", "capacity_level", "charge_full",
    "charge_now", "current_now", "cycle_count",
    "manufacturer", "model_name", "present",
    "serial_number", "status", "technology",
    "type", "voltage_min_design", "voltage_now"
]


def list_power_devices():
    devices = {}
    for root, dirs, files in os.walk(SYS_FS_DEVICES_TREE):
        for d in dirs:
            path = os.path.join(root, d)
            devices[d] = path

    return devices

def list_batteries():
    batteries = {}
    for root, dirs, files in os.walk(SYS_FS_DEVICES_TREE):
        for d in dirs:
            path = os.path.join(root, d)
            typepath = os.path.join(path, "type")
            if os.path.exists(typepath):
                with open(typepath, 'r') as f:
                    typ = f.read().strip()
                    if typ == "Battery":
                        batteries[d] = path

    return batteries

def battery_detail(batname):
    path = os.path.join(SYS_FS_DEVICES_TREE, batname)
    if not os.path.exists(path):
        raise BatteryNotFound("Battery {} not found".format(batname))

    bat = {}
    for f in FILES_TO_READ:
        p = os.path.join(path, f)
        if os.path.exists(p):
            with open(p, 'r') as readin:
                bat[f] = readin.read().strip()
    return bat

def battery_details():
    b = list_batteries()
    data = {}
    for bat, path in b.keys():
        data[bat] = battery_detail(bat)

    return b
