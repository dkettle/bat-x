BAT-X: Battery Control Application
----------------------------------

Name is a working title, subject to change.

BAT-X is the base battery software for setting battery settings and utilizing 
battery modules that may be required, such as tp-smapi and tpacpi-bat for 
setting charge limits and battery switching.

There are 3 ui wrappers planned around the base:

  * gtk system tray
  * kde system tray
  * kde plasma widget for desktop/system tray

KDE and Thinkpads are the main focus and some functionality may be limited
outside this scope, but as long as basic polling/battery switching is
implemented, it can be extended to other environments.

There are 3 primary ways to determine when BAT-X should act on battery
charge levels:

  * polling the /sys tree
  * inotify the /sys tree
  * use of module configuration and scripts

Determining when a battery is connected can be done with polling, but should
be done with udev/systemd. I don't want to rely strictly on systemd.

